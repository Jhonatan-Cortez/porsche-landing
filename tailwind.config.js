let colors = require('tailwindcss/colors')
delete colors['lightBlue']

module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      customBlue: '#1f336c',
      ...colors,
    },
    fontFamily: {
      sans: ['Roboto', 'sans-serif']
    },
    extend: {},
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
